package edu.vanier.dialogboxlab.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import edu.vanier.dialogboxlab.R;

public class DialogNewDog extends DialogFragment implements View.OnClickListener{

    public static final String TAG_NEW_DOG = "TAG_NEW_DOG";
    private OnDogActionListener listener;
    private EditText txtDogName;

    public DialogNewDog() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context != null){
            if(listener == null){
                listener = (OnDogActionListener)context;
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root =  inflater.inflate(R.layout.dialogue_new_dog, container, false);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Button submitBtn = (Button) view.findViewById(R.id.submitBtn);
        submitBtn.setOnClickListener(this);

        Button cancelBtn = (Button) view.findViewById(R.id.cancelBtn);
        cancelBtn.setOnClickListener(this);

        txtDogName = view.findViewById(R.id.newDogName);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.cancelBtn){
            this.dismiss();
        }
        else if(v.getId() == R.id.submitBtn){
            String dogName = txtDogName.getText().toString();
            listener.onNewDogSubmitted(dogName);
        }
    }
}
