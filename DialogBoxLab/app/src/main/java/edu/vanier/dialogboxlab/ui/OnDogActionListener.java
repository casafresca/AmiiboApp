package edu.vanier.dialogboxlab.ui;

public interface OnDogActionListener {

    void onNewDogSubmitted(String newDogName);
}
