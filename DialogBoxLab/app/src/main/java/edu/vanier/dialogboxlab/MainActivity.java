package edu.vanier.dialogboxlab;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import edu.vanier.dialogboxlab.ui.DialogNewDog;
import edu.vanier.dialogboxlab.ui.OnDogActionListener;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, OnDogActionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button dogBtn = (Button) findViewById(R.id.newDogBtn);
        dogBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Toast.makeText(this, "New Dog", Toast.LENGTH_SHORT).show();
        showNewDogDialog();
    }

    private void showNewDogDialog() {
        FragmentManager frg = getSupportFragmentManager();
        DialogNewDog newD = new DialogNewDog();
        newD.show(frg, DialogNewDog.TAG_NEW_DOG);
    }


    @Override
    public void onNewDogSubmitted(String newDogName) {
        Toast.makeText(this, "You entered "+newDogName, Toast.LENGTH_SHORT).show();
    }
}
