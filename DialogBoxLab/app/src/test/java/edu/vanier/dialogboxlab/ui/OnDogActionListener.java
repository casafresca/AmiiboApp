package edu.vanier.dialogboxlab.ui;

interface OnDogActionListener {
    void onNewDogSubmitted(String newDogName);
}
