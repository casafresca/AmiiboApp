package com.example.recyclertrial;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.LinkedList;

public class WordListAdapter extends RecyclerView.Adapter  {
    private LinkedList<String> mWordList;
    private LayoutInflater mInflater;

    public WordListAdapter(LinkedList<String> mWordList, Context ctx) {
        this.mWordList = mWordList;
        this.mInflater =  LayoutInflater.from(ctx);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = mInflater.inflate(R.layout.layout_item, viewGroup, false);
        return new WorldListViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        ((WorldListViewHolder)viewHolder).mWord.setText(mWordList.get(position));
    }

    @Override
    public int getItemCount() {
        return mWordList.size();
    }

    class WorldListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView mWord;

        public WorldListViewHolder(@NonNull View itemView) {
            super(itemView);
            mWord = itemView.findViewById(R.id.textView);
            mWord.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getLayoutPosition();

            String clicked = mWordList.get(position) + "*";
            mWordList.set(position, clicked);
            notifyDataSetChanged();
        }
    }
}
