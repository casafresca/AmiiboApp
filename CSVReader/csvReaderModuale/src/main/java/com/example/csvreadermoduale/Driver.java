package com.example.csvreadermoduale;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Driver {
    public static void main(String[] args) {
        System.out.println("Hello!");

        try {
            CSVReader reader = new CSVReader(new FileReader("data/nobleprizes.csv"));
            String [] nextLine;
            while ((nextLine = reader.readNext()) != null)
            {
                System.out.println(nextLine[0] + nextLine[1] + "etc...");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (CsvValidationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
