package com.example.applabrecycler;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.zip.Inflater;

public class Adapter extends RecyclerView.Adapter {
    private LinkedList <String> wordList;
    private LayoutInflater inflate;

    public Adapter(LinkedList<String> wordList, Context ctx) {
        this.wordList = wordList;
        this.inflate = LayoutInflater.from(ctx);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = inflate.inflate(R.layout.layout_item, viewGroup, false);
        return new ListViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

    }

    @Override
    public int getItemCount() {
        return wordList.size();
    }

    class ListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView word;

        public ListViewHolder(@NonNull View itemView) {
            super(itemView);
            word = itemView.findViewById(R.id.textView);


        }

        @Override
        public void onClick(View v) {

        }
    }


}
